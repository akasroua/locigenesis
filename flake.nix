{
  description = ''
    locigenesis is a tool that generates a human T-cell receptor (TCR), runs
    it through a sequence reader simulation tool and extracts CDR3.
  '';

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in { devShell = import ./shell.nix { inherit pkgs; }; });
}
