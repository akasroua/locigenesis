#!/bin/sh

usage() {
	echo "usage: generation.sh <number of sequences> <number of reads>"
	exit 1
}

if [ $# != 2 ]; then
	usage
fi

sequences=$1
number_of_reads=$2
data_directory="data/"
fastq=".fastq"
filename="sequence"
prefix="curesim_"

Rscript src/repertoire.r "$sequences" "$number_of_reads" &&
	CuReSim -f "$data_directory$filename$fastq" -o "$data_directory$prefix$filename$fastq"
Rscript src/alignment.r
rm "$data_directory/log.txt"
