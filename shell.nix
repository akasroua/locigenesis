{ pkgs ? import <nixpkgs> { } }:

with pkgs;

let
  CuReSim = stdenv.mkDerivation rec {
    name = "CuReSim";
    version = "1.3";
    src = fetchzip { url =
        "http://www.pegase-biosciences.com/wp-content/uploads/2015/08/${name}${version}.zip";
      sha256 = "1hvlpgy4haqgqq52mkxhcl9i1fx67kgwi6f1mijvqzk0xff77hkp";
      stripRoot = true;
      extraPostFetch = ''
        chmod go-w $out
      '';
    };

    nativeBuildInputs = [ makeWrapper ];

    installPhase = ''
      mkdir -pv $out/share/java $out/bin
      cp -r ${src} $out/share/java/${name}
      makeWrapper ${jre}/bin/java $out/bin/CuReSim --add-flags "-jar $out/share/java/${name}/${name}.jar"
    '';
  };
in mkShell {
  buildInputs =
    [ R rPackages.immuneSIM rPackages.Biostrings rPackages.stringr CuReSim ];
}
