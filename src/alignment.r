library(Biostrings)
library(parallel)

#' Import and process the TCR and VJ sequences
#'
#' @param file A file path with the sequences after applying a read simulator
#' @return A \code{list} with the TCR sequences and VJ sequences
parse_data <- function(file) {
  reversed_sequences <- Biostrings::readQualityScaledDNAStringSet(file)
  sequences <- Biostrings::reverseComplement(reversed_sequences)
  vj_segments <- union(
    readRDS("data/v_segments.rds"),
    readRDS("data/j_segments_phe.rds")
  )
  return(list(sequences, vj_segments))
}

#' Extracts the VJ metadata from the sequences read identifier
#'
#' @param metadata The read identifier of a sequence
#' @return A \code{list} with the V and J gene identifier
parse_metadata <- function(metadata) {
  id_elements <- unlist(strsplit(metadata, split = " "))
  v_identifier <- id_elements[2]
  j_identifier <- id_elements[3]
  return(list(v_id = v_identifier, j_id = j_identifier))
}

#' Fetches the sequence that matches the VJ gene identifier
#'
#' @param names The names of the VJ sequences
#' @param vdj_segments A \code{DNAStringSet} containing the VJ sequences
#' @param id The read identifier of a sequence
#' @return A \code{character} containing the gene sequence
match_id_sequence <- function(names, vdj_segments, id) {
  matches <- grep(names, pattern = id)
  if(id == "TRBJ2-2"){
    row <- matches[2]
  } else {
    row <- matches[1]
  }
  return(as.character(vdj_segments[row]))
}

#' Gets the V and J sequences for a particular read identifier
#'
#' @param metadata The read identifier of a sequence
#' @param names The names of the VJ sequences
#' @param vdj_segments A \code{DNAStringSet} containing the VJ sequences
#' @return A \code{list} with the V and J sequences
get_vj_sequence <- function(metadata, names, vdj_segments) {
  identifiers <- parse_metadata(metadata)
  v_sequence <- match_id_sequence(names, vdj_segments, id = identifiers["v_id"])
  j_sequence <- match_id_sequence(names, vdj_segments, id = identifiers["j_id"])
  return(list(v_seq = v_sequence, j_seq = j_sequence))
}

#' Obtains the VJ sequences for all the TCR sequences
#'
#' @param sequences A \code{QualityScaledDNAStringSet} with the TCR sequences
#' @param vdj_segments A \code{DNAStringSet} containing the VJ sequences
#' @return A \code{data.frame} with the V and J sequences
fetch_vj_sequences <- function(sequences, vdj_segments) {
  vj_sequences <- sapply(names(sequences),
    names(vdj_segments),
    vdj_segments,
    FUN = get_vj_sequence
  )
  results <- data.frame(t(vj_sequences))
  return(results)
}

#' Perform a pairwise alignment of a sequence with the canonical V or J sequence
#'
#' @param sequence A \code{DNAString} containing the TCR sequences
#' @param vdj_segment A \code{DNAString} containing the V or J sequence
#' @return A \code{PairwiseAlignments}
align_sequence <- function(sequence, vdj_segment) {
  return(Biostrings::pairwiseAlignment(
    subject = sequence,
    pattern = vdj_segment,
    type = "global-local",
    gapOpening = 1
  ))
}

#' Computes the coordinate shift of the Cysteine due to indels
#'
#' @param insertion An \code{IRanges} containing the insertions
#' @param deletion An \code{IRanges} containing the deletions
#' @param cys A \code{list} with the Cysteine coordinates
#' @param alignment A \code{PairwiseAlignments}
#' @return A \code{list} with the delta of the Cysteine coordinates
handle_indels <- function(insertion, deletion, cys, alignment) {
  ins_start <- sum(Biostrings::width(deletion[start(deletion) <= cys$start]))
  ins_end <- sum(Biostrings::width(deletion[end(deletion) <= cys$end]))
  shift_num <- c(0, cumsum(Biostrings::width(insertion))[-length(ins_start)])
  shifted_ins <- IRanges::shift(insertion, shift_num)
  gaps <- sum(width(shifted_ins[end(shifted_ins) < cys$start + ins_start])) +
    nchar(stringr::str_extract(alignedSubject(alignment), "^-*"))
  return(list("start" = ins_start - gaps, "end" = ins_end - gaps))
}

#' Find the coordinates of the first Cysteine of the HVR
#'
#' @param alignment A \code{PairwiseAlignments}
#' @return A \code{list} with the Cysteine coordinates
get_cys_coordinates <- function(alignment) {
  cys <- list("start" = 310, "end" = 312)
  insertion <- unlist(Biostrings::insertion(alignment))
  deletion <- unlist(Biostrings::deletion(alignment))
  delta_coordinates <- handle_indels(insertion, deletion, cys, alignment)
  read_start <- unlist(start(Biostrings::Views(alignment)))
  cys_start <- cys$start + delta_coordinates$start + read_start - 1
  cys_end <- cys$end + delta_coordinates$end + read_start
  return(list("start" = cys_start, "end" = cys_end))
}

#' Delimit the hypervariable region (HVR) for each TCR sequence
#'
#' @param sequences A \code{QualityScaledDNAStringSet} with the TCR sequences
#' @param vdj_segments A \code{DNAStringSet} containing the VJ sequences
#' @param cores Number of cores to apply multiprocessing
#' @return A \code{QualityScaledDNAStringSet} containing the HVR
get_hvr_sequences <- function(sequences, vdj_segments, cores = detectCores()) {
  df <- fetch_vj_sequences(sequences, vdj_segments)
  v_alignment <- parallel::mcmapply(sequences,
    df$v_seq,
    FUN = align_sequence,
    mc.cores = cores
  )
  cys_coordinates <- parallel::mclapply(v_alignment, FUN = get_cys_coordinates)
  cys_df <- as.data.frame(do.call(rbind, cys_coordinates))
  remaining <- Biostrings::subseq(sequences, start = unlist(cys_df$end) + 1)
  j_alignment <- parallel::mcmapply(remaining,
    df$j_seq,
    FUN = align_sequence,
    mc.cores = cores
  )
  j_start <- parallel::mclapply(
    j_alignment,
    function(x) start(Biostrings::Views(x)),
    mc.cores = cores
  )
  hvr_start <- unlist(cys_df$start)
  hvr_end <- unlist(cys_df$start) + unlist(j_start) + 2
  hvr <- Biostrings::subseq(sequences, start = hvr_start, end = hvr_end)
  return(hvr)
}

data <- parse_data(file = "data/curesim_sequence.fastq")
hvr <- get_hvr_sequences(sequences = data[[1]], vdj_segments = data[[2]])
Biostrings::writeXStringSet(hvr, "data/curesim-HVR.fastq", format = "fastq")
